#!/usr/bin/env bash

mail=$1

echo "mail : $mail"

if [[ "$mail" =~ "@" ]]; then
  echo "mail ok"
else
  echo "mail not ok"
  exit 1
fi

exit 0
